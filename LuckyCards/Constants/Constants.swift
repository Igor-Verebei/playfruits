
// MARK: - Key Values

let keyArtworkURL = "artwork_url"
let keyTracks = "tracks"
let keyID = "id"

// MARK: - Default Values

let defaultGridSize = 4 * 4
let defaultAlertTitle = "Good job!"
let defaultAlertMessage = "Want to play again?"
let nameAlertMessage = "Enter your name"

