
import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var playOutlet: UIButton!
    @IBOutlet weak var recordsOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        setOutlets()
    }
    
    func setOutlets() {
        nameLabel.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        playOutlet.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        playOutlet.roundCorners()
        recordsOutlet.dropShadow(color: .black, offSet: CGSize(width: 10, height: 10), radius: 20)
        recordsOutlet.roundCorners()
    }
    
    @IBAction func playButton(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "GameController") as? GameController else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func recordsButton(_ sender: UIButton) {
        let vc = RecordsViewController.loadFromNib()
        present(vc, animated: true, completion: nil)
    }
}

