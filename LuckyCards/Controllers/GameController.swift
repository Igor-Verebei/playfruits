
import UIKit

class GameController: UIViewController {
    
    @IBOutlet weak var playAgainOutlel: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var timer: UILabel!
    var name = ""
    var counter = 0
    var time = Timer()
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
    
    let game = MemoryGame()
    var cards = [Card]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        game.delegate = self
        collectionView.isHidden = true
        playAgainOutlel.isHidden = true
        
        DispatchQueue.main.async {
            
            self.showEnterNameAlert()
            
            GameManager.shared.getCardImages { [weak self] (cardsArray, error) in
                guard let self = self else {return}
                if let _ = error {
                }
                self.cards = cardsArray!
                self.setupNewGame()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if game.isPlaying {
            resetGame()
        }
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setupNewGame() {
        cards = game.newGame(cardsArray: self.cards)
        collectionView.reloadData()
    }
    
    func resetGame() {
        game.restartGame()
        setupNewGame()
    }
    
    func saveRecord() {
        
        if self.name.count != 0 {
            
            var recordsArray = RecordsManager.shared.getRecords()
            let record = RecordModel(name: self.name , time: self.counter)
            recordsArray.insert(record, at: 0)
            RecordsManager.shared.setSettings(recordsArray)
        }
    }
    
    @IBAction func onStartGame(_ sender: Any) {
        collectionView.isHidden = false
        setupNewGame()
        counter = 0
        timer.text = "0:00"
        
        time.invalidate()
        time = Timer.scheduledTimer(
            timeInterval: 1.0,
            target: self,
            selector: #selector(timerAction),
            userInfo: nil,
            repeats: true
        )
    }
    
    @objc func timerAction() {
        counter += 1
        timer.text = "0:\(counter)"
    }
}

// MARK: - CollectionView Delegate Methods
extension GameController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCell", for: indexPath) as? CardCell else {return UICollectionViewCell()}
        cell.showCard(false, animted: true)
        
        guard let card = game.cardAtIndex(indexPath.item) else { return cell }
        cell.card = card
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CardCell
        if cell.shown { return }
        game.didSelectCard(cell.card)
        collectionView.deselectItem(at: indexPath, animated:true)
    }
    
    // Collection view flow layout setup
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = Int(sectionInsets.left) * 4
        let availableWidth = Int(view.frame.width) - paddingSpace
        let widthPerItem = availableWidth / 4
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

// MARK: - MemoryGameProtocol Methods
extension GameController: MemoryGameProtocol, UITextFieldDelegate {
    
    func memoryGameDidStart(_ game: MemoryGame) {
        self.collectionView.reloadData()
    }
    
    func memoryGame(_ game: MemoryGame, showCards cards: [Card]) {
        for card in cards {
            guard let index = game.indexForCard(card) else { continue }
            let cell = collectionView.cellForItem(at: IndexPath(item: index, section:0)) as! CardCell
            cell.showCard(true, animted: true)
        }
    }
    
    func memoryGame(_ game: MemoryGame, hideCards cards: [Card]) {
        for card in cards {
            guard let index = game.indexForCard(card) else { continue }
            let cell = collectionView.cellForItem(at: IndexPath(item: index, section:0)) as! CardCell
            cell.showCard(false, animted: true)
        }
    }
    
    func memoryGameDidEnd(_ game: MemoryGame) {
        time.invalidate()
        
        let alertController = UIAlertController(title: defaultAlertTitle, message: defaultAlertMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] (action) in
            self?.saveRecord()
            self?.navigationController?.popToRootViewController(animated: true)
        }
        let playAgainAction = UIAlertAction(title: "Again", style: .default) { [weak self] (action) in
            self?.saveRecord()
            self?.onStartGame(UIAlertAction.self)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(playAgainAction)
        
        self.present(alertController, animated: true) { }
        
        resetGame()
    }
    
    func showEnterNameAlert() {
        let enterNameAlert = UIAlertController(title: nil, message: nameAlertMessage, preferredStyle: .alert)
        enterNameAlert.addTextField { (textField) in
        }
        
        let okAction = UIAlertAction(title: "Play", style: .default) { [weak self] (_) in
            guard let self = self else {return}
            if let name =  enterNameAlert.textFields?.first?.text {
                self.name = name
            }
            self.onStartGame(UIAlertAction.self)
            self.playAgainOutlel.isHidden = false
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { [weak self] (_) in
            self?.navigationController?.popToRootViewController(animated: true)
        }
        enterNameAlert.addAction(okAction)
        enterNameAlert.addAction(cancelAction)
        self.present(enterNameAlert, animated: true, completion: nil)
    }
}



