
import Foundation

class RecordModel: NSObject, Codable {
    
    var name: String?
    var time: Int?
    
    init (name: String, time: Int){
        self.name = name
        self.time = time
    }
    
    public enum CodingKeys: String, CodingKey {
        case name, time
    }
    
    public override init(){}
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.name, forKey: .name)
        try container.encode(self.time, forKey: .time)
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.time = try container.decodeIfPresent(Int.self, forKey: .time)
    }

}

